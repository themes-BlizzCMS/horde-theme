# Horde-Theme

Tema para BlizzCMS-Plus basado en la Facción de la Horda de World of Warcraft. 
https://wow-cms.com/en/ - https://gitlab.com/WoW-CMS

# Project

* https://wow-cms.com/en/
* https://gitlab.com/WoW-CMS

# Installation

* Add the "horde" folder in /application/themes/
* Then go to the admin panel and rename the theme to "horde" save changes and you're good to go.

# Requirements

* BlizzCMS-Plus

# Author

* @Black_Tyrael

# Screenshots

![Screenshot](Screenshot.png)